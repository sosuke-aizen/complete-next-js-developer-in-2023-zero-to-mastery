

####  **Problem**

If you are using React 18, you will notice that coffee stores are getting
created 2 times in Airtable.

  

####  **Reasoning:**

This is because in `next.config.js`, we have `reactStrictMode: true` enabled.

This enables React Strict Mode. You can read about it here:
<https://reactjs.org/blog/2022/03/29/react-v18.html#new-strict-mode-behaviors>
and this is a known issue with Next and React:

<https://github.com/vercel/next.js/issues/35822>  

Before this change, React would mount the component and create the effects:

    
    
    * React mounts the component.
      * Layout effects are created.
      * Effects are created.

With Strict Mode in React 18, React will simulate unmounting and remounting
the component in development mode:

    
    
    * React mounts the component.
      * Layout effects are created.
      * Effects are created.
    * React simulates unmounting the component.
      * Layout effects are destroyed.
      * Effects are destroyed.
    * React simulates mounting the component with the previous state.
      * Layout effects are created.
      * Effects are created.

  

You are seeing 2 coffee stores created as your page is getting unmounted and
remounted twice.

#### **Solution:**

If you turn off `reactStrictMode: false` and restart your server, you will
only see coffee store getting created once or you can live with it knowing
this is react 18 issue.

