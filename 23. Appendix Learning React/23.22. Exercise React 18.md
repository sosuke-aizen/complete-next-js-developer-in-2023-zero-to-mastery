

Now that you know how to update your project dependencies to the latest
versions, it is time to try it yourself.

  

[ **React version 18 was recently
announced!**](https://reactjs.org/blog/2022/03/29/react-v18.html) **** Luckily
for us there are no major changes, so go ahead and update the `react` and
`react-dom` packages to v18 in your `package.json` file and see if everything
still works!  
  
In the next video I will show you how I updated to React v17, and your
challenge is to do the same and upgrade the project to React v18 after seeing
what I do!

