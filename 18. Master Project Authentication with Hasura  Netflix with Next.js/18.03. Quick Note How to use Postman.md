

 **What is Postman?**

It is a tool to invoke your API's. Since you can invoke only GET requests in
the browser, you need a client like POSTMAN. Postman enables you to create and
send API requests. Send a request to an endpoint, retrieve data from a data
source, or test an API's functionality. You don't need to enter commands in a
terminal or write any code. Create a new request and select **Send** , and the
API response appears right inside Postman.

You can download Postman directly here:
[`https://www.postman.com/`](https://www.postman.com/)

 **How to use it:**

You can invoke the URL directly to use Postman. **You don't need to configure
any environment to use this**. Take a look at the docs to see how to use it:
<https://learning.postman.com/docs/getting-started/sending-the-first-request/>

  

Thanks to our community student [Alex](https://www.udemy.com/user/alex-17855/)
for pointing this out and helping improve this course!  

